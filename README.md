#SBlog简介
Sblog采用Spring ＋ SpringMVC ＋ Velocity ＋ Sql2o 完成。SBlog具有以下简单的功能：管理文章、管理分类、管理用户、管理About页面。文章编辑器采用MarkDown格式。

##安装方法:
1. 导入Maven项目
2. ```CREATE DATABASE SBLOG;```
3. ```use SBLOG;```
4. ```source SBLOG.sql```(导入时要在SBLOG.sql文件夹下进入mysql)

##SBlog网址
- 演示网址:[www.xingfly.com](http://www.xingfly.com)

- 本人专科18岁小菜鸟（今年大一）经过两位前辈指点，从只懂Java基础到用Servlet做出Blog，再到SpringMVC改进Blog.

- 🙏感谢[王爵](http://biezhi.me "王爵")、[jack_bj](http://www.codingyun.com "jack_bj")、冰神(icepoint℃冰)的帮助。

##注意事项

- 用户名:admin 用户密码:admin

- 不要随便删除用户,每个用户和文章有关联。删除用户后文章就不能显示！

- 友情链接请在编辑关于页面中手动添加。

- 图片存储采用七牛云存储，手动添加图片链接在文章中即可显示图片。

- 功能还不完善，还有待改进。发现Bug请联系549052145@qq.com

##网页展示
![show](http://7b1gp4.com1.z0.glb.clouddn.com/springmvc-QQ20160303-0%402x.png)
![show](http://7b1gp4.com1.z0.glb.clouddn.com/springmvc-QQ20160303-1%402x.png)
![show](http://7b1gp4.com1.z0.glb.clouddn.com/springmvc-QQ20160303-2%402x.png)
![show](http://7b1gp4.com1.z0.glb.clouddn.com/springmvc-QQ20160303-3%402x.png)
![show](http://7b1gp4.com1.z0.glb.clouddn.com/springmvc-QQ20160303-4%402x.png)
![show](http://7b1gp4.com1.z0.glb.clouddn.com/springmvc-QQ20160303-5%402x.png)
![show](http://7b1gp4.com1.z0.glb.clouddn.com/springmvc-QQ20160303-6%402x.png)

##喝了这碗鸡汤
如果你和我一样只有零星的Java基础，想做东西又感觉什么都做不出来。那么希望你能调整看书看视频和敲代码的时间。多多动手实践，慢慢进步。很多时候你以为你会了并不是你真的会了，只是你以为你会了。只有你可以动手写出来,才是你真的会了。










